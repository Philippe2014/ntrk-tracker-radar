#!/usr/bin/env python3
#Title   : NoTrack Tracker Radar Processor
#Description : This script will process DuckDuckGo Tracker Radar list into five seperate files for use in a DNS server like NoTrack and PiHole.
#              Lists are categorised as Confirmed, High, Medium, Low, and Unknown based on the certainty of a domain being a tracker
#Author  : QuidsUp
#Date    : 2020-06-28
#Version : 0.3
#Usage   : git clone https://github.com/duckduckgo/tracker-radar.git
#          python3 ntrk_tracker_radar.py

#Standard Imports
#from pprint import pprint
import datetime
import json
import os
import re


#Categories to exclude which have a purpose other than tracking
#Categories are taken from [https://github.com/duckduckgo/tracker-radar/blob/master/docs/CATEGORIES.md]
excluded_categories = {
    'CDN',
    'Embedded Content',
    'Federated Login',
    'Non-Tracking',
    'Online Payment',
    'Social Network',
    'SSO',
}

#Domains to exclude from complete blocking as they will cause noticeable breakage
excluded_domains = {
    'amazon-adsystem.com',             #Essential for Amazon Android / iPhone app
    #'azureedge.net',                  #Azure cloud services, hosts many legitimate domains
    'cdn-apple.com',                   #CDN
    'cloudfront.net',                  #CDN
    'cloudflare.net',                  #CDN
    'ebay.com',
    'ebaycdn.net',
    'hcaptcha.com',                    #Bot Protection, used on login screens
    'spiceworks.com',
    'spotifycdn.com',
    'walmart.com',                     #US Supermarket, also used by asda.co.uk
    'yahoo.co.jp',
    'zip.co',                          #Payments
}

tracker_categories = {                 #Known Tracker categories
    'Action Pixels',
    'Ad Motivated Tracking',
    'Analytics',
    'Third-Party Analytics Marketing',
}

#Regex to identify known tracking subdomains
#analytics, beacon / beacons, click, heatmap / heatmaps, insights, log / logger / logging, metrics / pixel, pixl / px / pix / pxl, stat / stats
#telemetry, track / trck / tracker / tracking, trk (plus optional digit)
#Optional domain .mtrcs, srvcs
#Followed by optional 2-3 char domain
Regex_Tracker = re.compile(r'^(analytics|beacons?|click|heatmaps?|insights|log(ger|ing)?|s?metri[ck][as]|pixel|pi?xl?|stats?|telemetry|tra?ck(er|ing)?|trk\d?)(\.mtrcs|srvcs)?(\.\w{2,3})?$')

#Regex to identify known advertising domains by beginning subdomain
#ads / adv, advertising, bid, tag. Followed by optional 2-3 char domain
Regex_Advertising = re.compile(r'^(ad(s|v)|advertising|bid|tag)(\.\w{2,3})?$')


processed_domains = set()         #Prevent duplication

certain_domains = []              #Confirmed Tracker / Advertising / Malware Domains
high_domains = []
med_domains = []
low_domains = []
unknown_domains = []

def add_certain(domain: str, owner: str, category: str) -> None:
    """
    Add a domain to certain_domains as long as it hasn't already been added
    """
    global processed_domains, certain_domains

    if domain in processed_domains:
        return

    processed_domains.add(domain)
    certain_domains.append(tuple([domain, owner, category]))


def add_high(domain: str, owner: str, category: str) -> None:
    """
    Add a domain to high_domains as long as it hasn't already been added
    """
    global processed_domains, high_domains

    if domain in processed_domains:
        return

    processed_domains.add(domain)
    high_domains.append(tuple([domain, owner, category]))


def add_med(domain: str, owner: str, category: str) -> None:
    """
    Add a domain to med_domains as long as it hasn't already been added
    """
    global processed_domains, med_domains

    if domain in processed_domains:
        return

    processed_domains.add(domain)
    med_domains.append(tuple([domain, owner, category]))


def add_low(domain: str, owner: str, category: str) -> None:
    """
    Add a domain to low_domains as long as it hasn't already been added
    """
    global processed_domains, low_domains

    if domain in processed_domains:
        return

    processed_domains.add(domain)
    low_domains.append(tuple([domain, owner, category]))


def add_unknown(domain: str, owner: str, category: str) -> None:
    """
    Add a domain to unknown_domains as long as it hasn't already been added
    """
    global processed_domains, unknown_domains

    if domain in processed_domains:
        return

    processed_domains.add(domain)
    unknown_domains.append(tuple([domain, owner, category]))


def is_advertising(categories: set) -> bool:
    """
    Checks if categories include Advertising categories and not Excluded categories
    """
    if 'Advertising' in categories:
        if categories and categories & excluded_categories:
            return False
        else:
            return True

    return False


def is_tracker(categories: set) -> bool:
    """
    Checks if categories include Tracker categories and not Excluded categories
    """
    if categories and categories & tracker_categories:
        if categories and categories & excluded_categories:
            return False
        else:
            return True

    return False


def is_malware(categories: set) -> bool:
    """
    Checks if categories include Malware categories and not Excluded categories
    """
    if 'Malware' in categories:
        if categories and categories & excluded_categories:
            return False
        else:
            return True

    return False


def is_ignore(domain: str, categories: set) -> bool:
    """
    Checks if domain is in Ignore domains
    Checks Categories does not include Excluded categories
    """
    if domain in excluded_domains:
        return True

    if categories and categories & excluded_categories:
        return True
    else:
        return False

    return False


def is_advertising_subdomain(subdomain: str) -> bool:
    """
    Check if subdomain looks like its related to advertising
    """
    if Regex_Advertising.match(subdomain) is None:
        return False
    else:
        return True


def is_tracker_subdomain(subdomain: str) -> bool:
    """
    Check if subdomain looks like its related to tracking
    """
    if Regex_Tracker.match(subdomain) is None:
        return False
    else:
        return True


def examine_resources(resources: list) -> bool:
    """
    Look at the resources found by DDG to determine the purpose of a domain
     based on maximum fingerprinting and number of example sites.
    A number of unique domains over four and maximum fingerprint over two
     is indicative that the domain is likely related to ads/tracking.
    NOTE a list of domains is preferred over the count DDG provide.

    Parameters:
        Resources list from json data
    Returns:
        True - Domain is likely third-party ad/tracking
        False - Any tracking elements are for the first-party domain
    """
    max_fingerprint: int = 0
    unique_domains = set()

    for resource in resources:
        max_fingerprint = max(max_fingerprint, resource.get('fingerprinting', 0))
        if 'exampleSites' not in resource:
            continue
        for site in resource['exampleSites']:
            if site.startswith('www.'):                #Not interested in www.
                continue
            unique_domains.add(site)

    #Have the thresholds of ad/tracking been met?
    if len(unique_domains) > 4 and max_fingerprint > 2:
        return True
    else:
        return False


def process_subdomains(top_private_domain: str, subdomains: list) -> bool:
    """
    Process subdomains
    Check if any subdomains match advertising or tracker regex patterns

    Parameters:
        Domain Name
        List of subdomains
    Returns:
        True: At least 1 tracker found in subdomain list
        False: Nothing useful found
    """
    containstracker: bool = False
    subdomain: str

    for subdomain in subdomains:
        if is_tracker_subdomain(subdomain):
            add_certain(f'{subdomain}.{top_private_domain}', 'First Party', 'Tracker')
            containstracker = True
        elif is_advertising_subdomain(subdomain):
            add_certain(f'{subdomain}.{top_private_domain}', 'First Party', 'Advertising')
            containstracker = True

    return containstracker


def process_lists() -> None:
    """
    Process Lists
    Sort by domain name a-z
    Display Results
    Send list to be saved to disk
    """
    #Sort lists by domain
    certain_domains.sort(key=lambda x: x[0])
    high_domains.sort(key=lambda x: x[0])
    med_domains.sort(key=lambda x: x[0])
    low_domains.sort(key=lambda x: x[0])
    unknown_domains.sort(key=lambda x: x[0])

    print('Results:')
    print('Confirmed Trackers : ', len(certain_domains))
    print('High Probability   : ', len(high_domains))
    print('Medium Probability : ', len(med_domains))
    print('Low Probability    : ', len(low_domains))
    print('Unknown Status     : ', len(unknown_domains))

    save_list(certain_domains, 'ddg_tracker_radar_confirmed.txt', 'Confirmed')
    save_list(high_domains, 'ddg_tracker_radar_high.txt', 'High')
    save_list(med_domains, 'ddg_tracker_radar_med.txt', 'Medium')
    save_list(low_domains, 'ddg_tracker_radar_low.txt', 'Low')
    save_list(unknown_domains, 'ddg_tracker_radar_unknown.txt', 'Unknown')

    save_hosts(certain_domains, 'ddg_tracker_radar_confirmed.hosts', 'Confirmed')
    save_hosts(high_domains, 'ddg_tracker_radar_high.hosts', 'High')
    save_hosts(med_domains, 'ddg_tracker_radar_med.hosts', 'Medium')
    save_hosts(low_domains, 'ddg_tracker_radar_low.hosts', 'Low')
    save_hosts(unknown_domains, 'ddg_tracker_radar_unknown.hosts', 'Unknown')


def process_json_file(filename: str) -> None:
    """
    Read a JSON file and process the domain data

    Parameters:
        JSON file to read
    Returns:
        None
    """
    domain: str
    owner: str
    fingerprinting: int
    categories: set
    data: dict

    data = json.load(open(filename, 'r'))              #Read JSON data from file

    #Check if file should be processed based on domain name
    domain = data.get('domain', '')
    if domain in excluded_domains or domain == '':
        return
    if domain.endswith('edgekey.net'):
        return

    #Convert categories list to a set
    categories = set(data.get('categories', []))

    #Set owner to either the value supplied by DDG or Third Party
    if 'owner' in data:
        owner = data['owner'].get('displayName', 'Third Party')
    else:
        owner = 'Third Party'

    #Re-adjust the fingerprinting score from DDG
    fingerprinting = data.get('fingerprinting', 0)

    #No categories suggests unknown or unclassifiable, be wary
    if len(categories) == 0:
        fingerprinting -= 1

    #Positive examination of resources suggests third-party ad/tracker
    if examine_resources(data.get('resources', [])):
        fingerprinting += 1

    #Unknown ownership may impact a legitimate domain, avoid setting as high
    if owner == 'Third Party' and fingerprinting >= 3:
        fingerprinting -= 1

    #Ignore domain or categories forces domain into unknown list
    if is_ignore(domain, categories):
        fingerprinting = 0

    #Process all CNAMEs
    process_cnames(data.get('cnames', []), owner, categories, fingerprinting)


    #1. Add known tracker categorisation to certain list
    if is_tracker(categories):
        add_certain(domain, owner, 'Tracker')
        return

    #2. Add known advertising categorisation to certain list
    if is_advertising(categories):
        add_certain(domain, owner, 'Advertising')
        return

    #3. Add known malware categorisation to certain list
    if is_malware(categories):
        add_certain(domain, owner, 'Malware')
        return

    #4. High certainty fingerprinting to high list
    if fingerprinting >= 3:
        add_high(domain, owner, 'Tracker High Certainty')
        return

    #5. Any tracking / advertising subdomains to certain list
    if process_subdomains(domain, data.get('subdomains', [])):
        return

    #6. Medium certainty to med list
    if fingerprinting == 2:
        add_med(domain, owner, 'Tracker Medium Certainty')

    #7. Low certainty to low list
    elif fingerprinting == 1:
        add_low(domain, owner, 'Tracker Low Certainty')

    #At this point no other match has taken place.
    else:
        add_unknown(domain, owner, 'Unknown Status')


def process_cnames(cnames: list, owner: str, categories: set, fingerprinting: int) -> None:
    """
    Process cnames
    """
    domain: str
    cname: dict
    is_parent_tracker: bool = is_tracker(categories)
    is_parent_advertising: bool = is_advertising(categories)
    is_parent_malware: bool = is_malware(categories)

    for cname in cnames:
        domain = cname['original']

        if is_ignore(domain, categories):
            continue

        #1. Add known tracker categorisation to certain list
        if is_parent_tracker:
            add_certain(domain, owner, 'Tracker')

        #2. Add known advertising categorisation to certain list
        elif is_parent_advertising:
            add_certain(domain, owner, 'Advertising')

        #3. Add known malware categorisation to certain list
        elif is_parent_malware:
            add_certain(domain, owner, 'Malware')

        #4. Add domain based on fingerprinting score to respective certainty
        elif fingerprinting >= 3:
            add_high(domain, owner, 'Tracker High Certainty')

        elif fingerprinting >= 2:
            add_med(domain, owner, 'Tracker Medium Certainty')

        elif fingerprinting == 1:
            add_low(domain, owner, 'Tracker Low Certainty')


def read_dir(domainsdir: str) -> None:
    """
    Find all files in folder and sub folders

    Parameters:
        Location of DDG Tracker Radar domains folder
    """
    file_count: int = 0                          #Number of files processed

    print(f'Reading json files from {domainsdir}')
    file_list = os.scandir(domainsdir)

    for root,dirs,files in os.walk(domainsdir):
        for f in files:
            filename = os.path.join(root, f)

            #Ignore anything other than a .json file
            if not filename.endswith('.json'):
                continue

            file_count += 1
            process_json_file(filename)


    print(f'Processed {file_count} files')
    print()

    process_lists()                             #Finally sort the list and then save to disk


def save_hosts(domains: list, filename: str, listtype: str) -> bool:
    """
    Save a blocklist as a hosts file format

    Parameters:
        List of data to save to file
        File to save
        List type for comments
    Returns:
        True on success
        False on error
    """
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d')

    try:
        f = open(filename, 'w')                        #Open file for ascii writing
    except IOError as e:
        print(f'Error writing to {filename}')
        print(e)
        return False
    except OSError as e:
        print(f'Error writing to {filename}')
        print(e)
        return False
    else:
        f.write(f'# Title : DuckDuckGo Tracker Radar\n')
        f.write(f'# Description : Domains processed by Ntrk_TrackerRadar from DuckDuckGo Tracker Rader categorised as {listtype}\n')
        f.write('# Author : QuidsUp\n')
        f.write('# License : GNU General Public License v3.0\n')
        f.write('# @GitLab : https://gitlab.com/quidsup/ntrk-tracker-radar\n')
        f.write(f'# Updated : {timestamp}\n')
        f.write('#===============================================================\n\n')

        for line in domains:
            f.write(f'0.0.0.0 {line[0]}\n')
        f.close()
    return True


def save_list(domains: list, filename: str, listtype: str) -> bool:
    """
    Save a blocklist as a plainlist format

    Parameters:
        List of data to save to file
        File to save
        List type for comments
    Returns:
        True on success
        False on error
    """
    timestamp = datetime.datetime.now().strftime('%Y-%m-%d')

    try:
        f = open(filename, 'w')                        #Open file for ascii writing
    except IOError as e:
        print(f'Error writing to {filename}')
        print(e)
        return False
    except OSError as e:
        print(f'Error writing to {filename}')
        print(e)
        return False
    else:
        f.write(f'# Title : DuckDuckGo Tracker Radar\n')
        f.write(f'# Description : Domains processed by Ntrk_TrackerRadar from DuckDuckGo Tracker Rader categorised as {listtype}\n')
        f.write('# Author : QuidsUp\n')
        f.write('# License : GNU General Public License v3.0\n')
        f.write('# @GitLab : https://gitlab.com/quidsup/ntrk-tracker-radar\n')
        f.write(f'# Updated : {timestamp}\n')
        f.write('#===============================================================\n\n')

        for line in domains:
            f.write(f'{line[0]} #{line[1]} - {line[2]}\n')
        f.close()
    return True

def main():
    print('NoTrack Tracker Radar Blocklist Processor')
    print()
    read_dir('../tracker-radar/domains/')


if __name__ == "__main__":
    main()

